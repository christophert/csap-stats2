/**
*   Christopher Tran / CSAP
*   10/13/13 12:52 PM
*   Commit by user tex on inapp.dynproc.it.chtr.us
*
*
*   QOTP:
 *  "Applications programming is a race between software engineers, who strive to produce idiot-proof programs, and the
 *  universe which strives to produce bigger idiots. So far the Universe is winning."
 *  -Rick Cook
 *
 *  Constants:
 *      MAXSTUDENTS: This is the max amount of students.
 *      *LOW: The low boundary for the (A/B/C/D) grade(s).
 *  Private Data:
 *      scoreLow: This is the initialization for the lowest score in the dataset.
 *      scoreHigh: This is the initialization for the highest score in the dataset.
 *      totalCount: This contains the total count of valid scores in the dataset.
 *      totalElements: This contains the total count of students in the array.
 *      sentinelCheck: This contains the sentinel value set by the doSentinelCheck function.
 *      filename: This contains the filename entered by the user.
 *
 *  Methods:
 *
 *      welcomeMessage():
 *          This method presents the welcome message.
 *      setFileName():
 *          This method receives input from the user consisting of the file
 *          desired to be read. It will confirm that the file string is not
 *          empty and that is exists. It then sets the variable "filename".
 *
 *      checkIfExists():
 *          This is a private method that assists the setFileName method in
 *          performing file existence checks. It will return either true or
 *          false, depending on the file's presence.
 *
 *      readFile(String path):
 *          This method performs the main functions of the program. It will
 *          receive the filename as an argument, and fill the array with
 *          Student objects with data from the file.
 *
 *      arraySort(Student[] a):
 *          This method receives a Student object array as an argument and sorts it
 *          according to the method provided by the user. Either by ID(1) or by score(0).
 *          It then returns the sorted array. Utilizes Bubble Sort Algorithm.
 *
 *      calcPercentage():
 *          This method will tally and calculate the amount of scores in each score
 *          range. It will then calculate the percentages of those scores to the total score.
 *          It will then print its calculations. It will also display a message regarding
 *          bad data if it was encountered.
 *
 *      *intCheck(String s):
 *          Private method that checks if the input is in integer.
 *
 *      doSentinelCheck():
 *          This method outputs a prompt to ask the user if they would like to
 *          restart the process.
 *
 *      getSentinelCheck():
 *          This method returns true or false based on the value of the sentinelCheck
 *          variable as set by doSentinelCheck().
 *
 *     calcMean():
 *          This method calculates the mean of the scores.
 *     calcMedian():
 *          This method calculates the median of the scores.
 *
 *     getStudentList():
 *          This method creates a loop and calls the getStudentInfo from the Student class for
 *          the printing of the roster.
 *
 *     promptSearch():
 *          This method will prompt the user asking if they would like to search for
 *          a specific user.
 *
 *     getStudentById(int id):
 *          This method will run a binary search on the studentArray sorted by ID and
 *          return the single student if one exists.
 *
 *     main(String args[]): Anything your heart desires.
**/
import java.io.*;
import java.util.StringTokenizer;

public class Roster {
    public static int MAXSTUDENTS = 30,
    ALOW = 90,
    BLOW = 80,
    CLOW = 70,
    DLOW = 60;

    private int scoreHigh = 0,
                scoreLow = 100,
                totalcount = 0,
                totalElements = 0;

    private Student[] studentArray = new Student[MAXSTUDENTS];
    private char sentinelCheck;

    private String filename;
    public Roster() {
    }

    public void welcomeMessage() {
        System.out.println("Welcome! This program will list the students in your database and the statistics of the students.");
    }

    public void setFileName() throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);

        do {
            System.out.print("Enter a filename: ");
            filename = input.readLine();

            if(filename.equals("") || filename == null || !checkIfExists()) {
                System.out.println("File not found or insufficient permissions. Try again.");
            }
        }
        while (filename.equals("") || filename == null || !checkIfExists());
    }


    private boolean checkIfExists() throws IOException {
        File check = new File(filename);
        return check.isFile();
    }

    public void readFile() throws IOException {
        String inputString;

        FileReader reader = new FileReader(filename);
        BufferedReader inFile = new BufferedReader(reader);

        inputString = inFile.readLine();
        int arrayLoc = 0;
        while (inputString != null) {
            StringTokenizer st = new StringTokenizer(inputString);
            while(st.hasMoreTokens()) {
                studentArray[arrayLoc] = new Student(Integer.parseInt(st.nextToken()),Integer.parseInt(st.nextToken()),st.nextToken().toUpperCase().charAt(0));
                arrayLoc++;
            }
            inputString = inFile.readLine();
        }
        totalElements = arrayLoc;
        studentArray = arraySort(studentArray, 0);
    }

    private static boolean intCheck(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public Student[] arraySort(Student[] a, int method) {
        if(method == 1) {
            int outer;
            Student temp;
            boolean swap = true;
            outer = totalElements;
            while(swap) {
                swap = false;
                outer--;
                for(int inner=0;inner < outer;inner++) {
                    if(a[inner].getScore() > a[inner+1].getScore()) {
                        temp = a[inner];
                        a[inner] = a[inner+1];
                        a[inner+1] = temp;
                        swap = true;
                    }
                }
            }
        }
        else {
            int outer;
            Student temp;
            boolean swap = true;
            outer = totalElements;
            while(swap) {
                swap = false;
                outer--;
                for(int inner=0;inner < outer;inner++) {
                    if(a[inner].getID() > a[inner+1].getID()) {
                        temp = a[inner];
                        a[inner] = a[inner+1];
                        a[inner+1] = temp;
                        swap = true;
                    }
                }
            }
        }
        return a;
    }

    public void calcPercentage() {
        int acount = 0,
                bcount = 0,
                ccount = 0,
                dcount = 0,
                fcount = 0,
                badDataCount = 0;
        boolean badData = false;

        for(int x=0;x<totalElements;x++) {
            int score = studentArray[x].getScore();
            if(studentArray[x].validScore()) {
                if(score > scoreHigh)
                    scoreHigh = score;
                if(score < scoreLow)
                    scoreLow = score;
                totalcount++;

                if(score >= ALOW)
                    acount++;
                else if(score >= BLOW)
                    bcount++;
                else if(score >= CLOW)
                    ccount++;
                else if(score >= DLOW)
                    dcount++;
                else
                    fcount++;
            }
            else
                badDataCount++; badData = true;

        }

        if(badData)
            System.out.println(badDataCount + " invalid scores was encountered during processing.");
        System.out.println("A Percentage: " + (double)acount / totalcount * 100 + "%");
        System.out.println("B Percentage: " + (double)bcount / totalcount * 100 + "%");
        System.out.println("C Percentage: " + (double)ccount / totalcount * 100 + "%");
        System.out.println("D Percentage: " + (double)dcount / totalcount * 100 + "%");
        System.out.println("F Percentage: " + (double)fcount / totalcount * 100 + "%");

    }

    public double calcMean() {
        int tempScores = 0;
        for(int x=0;x<totalElements;x++) {
            if(studentArray[x].validScore()) {
                tempScores += studentArray[x].getScore();
            }
        }
        return Math.round((double)tempScores / totalcount * 10.) / 10.;
    }

    public double calcMedian() {
        int mid = totalElements / 2;
        if(totalElements % 2 == 1)
            return studentArray[mid].getScore();
        else
            return (studentArray[mid-1].getScore() + studentArray[mid].getScore()) / 2.0;
    }

    public void getStudentList() {
        for(int x=0;x<totalElements;x++)
            studentArray[x].getStudentInfo();
    }

    public void promptSearch() throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);
        String tempInput;
        char optionChose;
        int enteredID;
        do {
            System.out.print("Would you like to search for a specific student? (Y/N): ");
            tempInput = input.readLine().toUpperCase();
            if(tempInput == null || tempInput.trim().equals("")) {
                System.out.println("Please choose Y/N");
            }
        } while (tempInput == null || tempInput.trim().equals(""));
        optionChose = tempInput.charAt(0);

        if(optionChose == 'Y') {
            do {
                System.out.print("Please enter the ID to search by: ");
                tempInput = input.readLine();
                if(tempInput == null || tempInput.trim().equals("") || !intCheck(tempInput)) {
                    System.out.println("Please enter a valid ID number.");
                }
            } while (tempInput == null || tempInput.trim().equals("") || !intCheck(tempInput));
            enteredID = Integer.parseInt(tempInput);

            getStudentById(enteredID);
        }
    }
    public void getStudentById(int id) {
        Student[] sortById;
        sortById = arraySort(studentArray, 0);
        int low=0;
        int high = totalElements;
        int mid = (low+high)/2;
        try {
            while(low<=high&&sortById[mid].getID() != id) {
                if(sortById[mid].getID() < id)
                    low = mid+1;
                else
                    high = mid - 1;
                mid=(low+high)/2;
            }
            if(low>high) {
                System.out.println("A student with the ID " + id + " was not found in the database.");
            }
            else {
                System.out.println("Student " + id + " has a score of " + sortById[mid].getScore() + " in the major " + sortById[mid].getMajorName());
            }
        } catch (NullPointerException e) {
            System.out.println("Student with the ID " + id + " was not found in the database.");
        }

    }

    //Sentinel Check
    public void doSentinelCheck() throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);
        String tempInput;
        do {
            System.out.print("Would you like search for another student? (Y/N): ");
            tempInput = input.readLine();
            if(tempInput == null || tempInput.trim().equals("")) {
                System.out.println("Please type Y or N.");
            }
        } while (tempInput == null || tempInput.trim().equals(""));
        tempInput = tempInput.toUpperCase();
        sentinelCheck = tempInput.charAt(0);
    }

    public boolean getSentinelCheck() {
        if(sentinelCheck=='Y')
            return true;
        else
            return false;
    }

    public static void main(String args[]) throws IOException {
        Roster roster = new Roster();

        roster.welcomeMessage();

        roster.setFileName();
        roster.readFile();

        roster.getStudentList();

        roster.calcPercentage();
        System.out.println("Mean scores: " + roster.calcMean());
        System.out.println("Median score: " + roster.calcMedian());

        do {
            roster.promptSearch();
            roster.doSentinelCheck();
        } while (roster.getSentinelCheck());
    }
}
/*
scores2.txt Search Output
Welcome! This program will list the students in your database and the statistics of the students.
Enter a filename: scores2.txt
Student 32 has a score of -10 in the major Art
Student 50 has a score of 8 in the major Mathematics
Student 63 has a score of 50 in the major Engineering
Student 85 has a score of 60 in the major Mathematics
Student 87 has a score of 70 in the major Business
Student 11 has a score of 70 in the major Engineering
Student 75 has a score of 80 in the major Computer Science
Student 17 has a score of 85 in the major Engineering
Student 53 has a score of 87 in the major Business
Student 65 has a score of 88 in the major Mathematics
Student 73 has a score of 89 in the major Education
Student 27 has a score of 90 in the major Incorrect Major Code
Student 93 has a score of 90 in the major Engineering
Student 33 has a score of 90 in the major Business
Student 43 has a score of 95 in the major Computer Science
Student 83 has a score of 95 in the major Mathematics
Student 55 has a score of 99 in the major Education
Student 44 has a score of 300 in the major Engineering
Student 39 has a score of 400 in the major Incorrect Major Code
Student 22 has a score of 500 in the major Art
4 invalid scores was encountered during processing.
A Percentage: 37.5%
B Percentage: 31.25%
C Percentage: 12.5%
D Percentage: 6.25%
F Percentage: 12.5%
Mean scores: 77.9
Median score: 88.5
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 0
A student with the ID 0 was not found in the database.
Would you like search for another student? (Y/N): y
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 11
Student 11 has a score of 70 in the major Engineering
Would you like search for another student? (Y/N): y
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 27
Student 27 has a score of 90 in the major Incorrect Major Code
Would you like search for another student? (Y/N): y
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 50
Student 50 has a score of 8 in the major Mathematics
Would you like search for another student? (Y/N): y
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 65
Student 65 has a score of 88 in the major Mathematics
Would you like search for another student? (Y/N): y
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 93
Student 93 has a score of 90 in the major Engineering
Would you like search for another student? (Y/N): y
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 100
Student with the ID 100 was not found in the database.
Would you like search for another student? (Y/N): y
Would you like to search for a specific student? (Y/N): y
Please enter the ID to search by: 25
A student with the ID 25 was not found in the database.
Would you like search for another student? (Y/N): n

Process finished with exit code 0
 */
