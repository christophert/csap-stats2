/**
*   Christopher Tran / CSAP
*   10/13/13 1:03 PM
*   Commit by user tex on inapp.dynproc.it.chtr.us
*
*
*   QOTP:
 *   "Applications programming is a race between software engineers, who strive to produce idiot-proof programs, and the
 *  universe which strives to produce bigger idiots. So far the Universe is winning."
 *  -Rick Cook
 *
 *  Constants:
 *      LOWBOUNDS: Lowest Score possible
 *      HIGHBOUNDS: Highest score possible
 *      MAJORCODES: All valid major codes
 *
 *  Private Data:
 *      id: Student ID
 *      score: Student Score
 *      major: Student Major code
 *
 *  Methods:
 *      getID(): returns ID
 *      getScore(): returns score
 *      getMajor(): returns Major code
 *      getMajorName(): returns Major Name from the major code
 *      validScore(): Determines if the student's score is invalid
 *      getStudentInfo(): Prints the student information for output.
 *
**/
public class Student extends Roster {
    public static int LOWBOUNDS = 0,
                    HIGHBOUNDS = 100;
    public static String MAJORCODES = "ABCEMN";

    int id,score;
    char major;

    public Student(int sId, int sS, char sM) {
        id = sId;
        score = sS;
        major = sM;

    }

    public int getID() {
        return id;
    }
    public int getScore() {
        return score;
    }
    public int getMajor() {
        return major;
    }

    public String getMajorName() {
        int majorLoc = MAJORCODES.indexOf(major);

        switch(majorLoc) {
            case 0:
                return "Art";
            case 1:
                return "Business";
            case 2:
                return "Computer Science";
            case 3:
                return "Education";
            case 4:
                return "Mathematics";
            case 5:
                return "Engineering";
            default:
                return "Incorrect Major Code";
        }
    }

    public boolean validScore() {

        return !(score < LOWBOUNDS || score > HIGHBOUNDS);
    }

    public void getStudentInfo() {
        System.out.println("Student " + id + " has a score of " + score + " in the major " + getMajorName());
    }
}
